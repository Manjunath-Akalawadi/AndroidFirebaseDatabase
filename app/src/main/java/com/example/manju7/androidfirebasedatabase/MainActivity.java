package com.example.manju7.androidfirebasedatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.firebase.client.Firebase;
import com.google.firebase.FirebaseApp;

public class MainActivity extends AppCompatActivity {

    Button button;

    Firebase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Firebase.setAndroidContext(this);

        firebase=new Firebase("https://androidfirebasedatabase-49ffc.firebaseio.com/");

        button=(Button)findViewById(R.id.button2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Firebase ref=firebase.child("name");
                ref.setValue("Manju");

                Firebase ref1=firebase.child("id");
                ref1.setValue("007");
            }
        });
    }
}
